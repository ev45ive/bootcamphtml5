        var userform = document.querySelector('#userform')
        var Zapisz = document.querySelector('#save')
        var Wczytaj = document.querySelector('#load')
        var imie = document.querySelector('#imie')
        var nazwisko = document.querySelector('#nazwisko')
        var coords = {};

        function Storage(persist = false){
            if(persist == true){
                this.storage = localStorage
            }else{
                this.storage = sessionStorage
            }
        }
        Storage.prototype = {
            set: function(key,value){
                 this.storage.setItem(key, JSON.stringify(value))
            },
            get: function(key){
                return JSON.parse(this.storage.getItem(key))
            }
        }
        var database = new Storage(false);

        userform.addEventListener('submit', function(event){
            var elements = event.target.elements

            var user = {
                imie : elements.imie.value,
                nazwisko: elements.nazwisko.value,
                location: coords
            }

            database.set('user', user)

            event.preventDefault();
        })

        Wczytaj.addEventListener('click', function(){
            var user = database.get('user')

            imie.value = user.imie;
            nazwisko.value = user.nazwisko;
            coords.latitude = user.location.latitude;
            coords.longitude = user.location.longitude;
            updateMap(coords)
        })

        // Zapisz.addEventListener('click', function(){
        //     localStorage.setItem('imie', imie.value )
        //     localStorage.setItem('nazwisko', nazwisko.value )
        // })



        var mapa = document.querySelector('#mapa')
        var update = document.querySelector('#update')

        function updateMap(coords){
            if(!coords.latitude || !coords.longitude){
                return;
            }
            var latitude = coords.latitude;
            var longitude = coords.longitude;

            mapa.src = `https://maps.googleapis.com/maps/api/staticmap?center=${latitude},${longitude}&size=300x300&zoom=15&markers=color:blue%7Clabel:A%7C52,21`
        }

    
        update.addEventListener('click', function(){
           navigator.geolocation.getCurrentPosition(function(info){
               coords.latitude = info.coords.latitude;
               coords.longitude = info.coords.longitude;

               updateMap(coords)
           });
        })
