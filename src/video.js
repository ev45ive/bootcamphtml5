     var video = document.getElementById('video');
        var currentTime = document.getElementById('currentTime');

        video.addEventListener('loadedmetadata', function(){
            currentTime.max = video.duration;
        })
        video.addEventListener('timeupdate', function(event){
            currentTime.value = event.target.currentTime;
        })
        currentTime.addEventListener('change', function(){
            video.currentTime = currentTime.value
        })



        var playBtn = document.getElementById('play')
        
        playBtn.addEventListener('click',function(){
            if(video.paused){
                video.play()
                playBtn.value = "Pause"
            }else{
                video.pause();
                playBtn.value = "Play"
            }
        })
