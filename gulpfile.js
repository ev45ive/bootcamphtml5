var gulp = require('gulp')
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var server = browserSync.create()
// gulp-load-plugins
// gulp-wiredep
// gulp-inject
// gulp-rename
// gulp-if /gulp-filter
// gulp-debug
// gulp-ngannotate
// gulp-ng-templates

gulp.task('hello', function(){
    console.log('Hello from Gulp!')
})

gulp.task('sass', function () {
  return gulp.src('./src/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist'));
});


gulp.task('build', function(){

    gulp.src(['./src/**/*.js', '!./src/**/*.test.js'])
        .pipe(concat('scripts.js'))
        .pipe( gulp.dest('./dist/'))

    gulp.src(['./src/**/*.html'])
        .pipe(gulp.dest('./dist'))

})

gulp.task('serve',['build','sass'], function(){
    server.init({
        server:{
            baseDir: './dist'
        },
        port:8080
    })

    gulp.watch(['./src/**/*.js'],['build'])
    gulp.watch(['./src/**/*.scss'],['sass'])
    gulp.watch(['./dist/**/*.js','./dist/**/*.css'], function(){
        server.reload();
    })
})

gulp.task('watch', function(){

    gulp.watch(['./src/**/*.js'],['build'])
    gulp.watch(['./src/**/*.scss'],['sass'])
})